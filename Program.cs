﻿using System;

namespace ProvaPratica
{
    class Program
    {
        static void Main(string[] args)
        {
            var corOriginal = Console.ForegroundColor;
            Console.Clear();
            Console.WriteLine("Validação de CPF / CNPJ\n");
            Console.Write("Digite um CPF ou CNPJ: ");
            string documento = Console.ReadLine();
            bool ok = (documento.Length == 11) ? Validador.CPF(documento) : Validador.CNPJ(documento);

            if (ok)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\nO documento é verdadeiro!\n");
                Console.ForegroundColor = corOriginal;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\nO documento é falso!\n");
                Console.ForegroundColor = corOriginal;
            }
        }
    }
}
